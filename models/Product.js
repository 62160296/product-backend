const mongoose = require('mongoose')
const { Schema } = mongoose
const productsSchema = Schema({
  name: String,
  price: Number
})

module.exports = mongoose.model('Product', productsSchema)
